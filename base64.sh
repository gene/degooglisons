#!/bin/sh
echo "{\n  \"gafam\": {\r"  > public/img/img.json
for image in public/img/gafam/*.png
do
  name=${image##*/}
  name=${name%.*}
  echo "    \"$name\": \"data:image/png;base64,$(base64 --wrap=0 $image)\"," >> public/img/img.json
done
truncate -s-2 public/img/img.json
echo "\n  },\n  \"leds\": {\r"  >> public/img/img.json
for image in public/img/leds/*.png
do
  name=${image##*/}
  name=${name%.*}
  echo "    \"$name\": \"data:image/png;base64,$(base64 --wrap=0 $image)\"," >> public/img/img.json
done
truncate -s-2 public/img/img.json
echo "\n  }\n}" >> public/img/img.json

